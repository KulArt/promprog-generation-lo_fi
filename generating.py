import sys
import cv2


out_file = open(sys.argv[2], "w+")

img = cv2.imread(sys.argv[1])
height, width, depth = img.shape

result =  f"int height = {height};\n"
result += f"int width = {width};\n"
result += f"unsigned char map[{height}][{width}][{depth}] = {{"
result += ",".join(["{" + ", ".join([f"{{{cur_pixel[0]}, {cur_pixel[1]}, {cur_pixel[2]}}}" for cur_pixel in line]) + "}" for line in img]) + "};"

out_file.write(result)
out_file.close()