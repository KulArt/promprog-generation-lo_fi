#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <iostream>


int main(int argc, char *argv[]) {
  std::string map_path(argv[1]);
  std::string img_path(argv[2]);
  cv::Mat map = imread(map_path, cv::IMREAD_COLOR);
  cv::Mat img = imread(img_path, cv::IMREAD_COLOR);

  for (int row = 0; row < img.rows; ++row) {
    for (int col = 0; col < img.cols; ++col) {
      auto& cur_pixel = img.at<cv::Vec3b>(row, col);
      for (int channel = 0; channel < img.channels(); ++channel) {
        cur_pixel[channel] = map.at<cv::Vec3b>(std::abs(img.channels() - 1 - channel), cur_pixel[channel])[channel];
      }
    }
  }
  auto out_path = std::string(argv[2]);
  out_path = out_path.substr(0, out_path.size() - 4) + "_out_first_way.png";

  imwrite(out_path, img);
}