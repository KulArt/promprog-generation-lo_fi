# Задание на кодогенерацию и фильтры из Инсты

В `run.sh` описана последовательность действий для сборки (накатите себе opencv) плюсовый и питоновский только. Пример работы.

До преображения:

![Before](test.png)

После преображения:

![After](test_out_first_way.png)


По времени результаты:
```
First way

real    0m0.055s
user    0m0.044s
sys     0m0.010s

Second way

real    0m0.048s
user    0m0.038s
sys     0m0.009s

```