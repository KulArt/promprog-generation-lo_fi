#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <iostream>

#include "map.h"

int main(int argc, char *argv[]) {
  std::string img_path(argv[1]);
  cv::Mat img = imread(img_path, cv::IMREAD_COLOR);
  for (int row = 0; row < img.rows; ++row) {
    for (int col = 0; col < img.cols; ++col) {
      auto& cur_pixel = img.at<cv::Vec3b>(row, col);
      for (int channel = 0; channel < img.channels(); ++channel) {
        cur_pixel[channel] = map[std::abs(img.channels() - 1 - channel)][cur_pixel[channel]][channel];
      }
    }
  }
  auto out_path = std::string(argv[1]);
  out_path = out_path.substr(0, out_path.size() - 4) + "_out_second_way.png";
  imwrite(out_path, img);
}