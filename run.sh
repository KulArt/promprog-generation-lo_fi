#!/bin/bash

mkdir build
cd build

cmake ..
make

cd ..


chmod a+x ./build/first
chmod a+x ./build/second

echo First way
time ./build/first map.png test.png
echo Second way
time ./build/second test.png