cmake_minimum_required(VERSION 3.0)
project(task4)

set(CMAKE_CXX_STANDARD 11)

add_custom_command(
    OUTPUT  ${PROJECT_SOURCE_DIR}/map.h
    COMMAND python3 ${PROJECT_SOURCE_DIR}/generating.py ${PROJECT_SOURCE_DIR}/map.png ${PROJECT_SOURCE_DIR}/map.h
    DEPENDS ${PROJECT_SOURCE_DIR}/generating.py
)

find_package(OpenCV REQUIRED)
include_directories(${OpenCV_INCLUDE_DIRS})

add_executable(first first.cpp)
target_link_libraries(first ${OpenCV_LIBS})

add_executable(second second.cpp map.h)
target_link_libraries(second ${OpenCV_LIBS})